# HTML5 & CSS3 Basics

## HTML & CSS
HTML (Hyper Text Markdown Language) documents are described by HTML tags, please check the example [blog](blog/). The basic structure of HTML is like:
```html
<!DOCTYPE html>
<html>
    <head>
        <title>...</title>
        ...
    </head>
    <body>...</body>
</html>
```

CSS (Cascading Style Sheets) defines exactly how HTML looks like, so HTML is content and CSS is style. There are 3 ways to use CSS:
1. CSS code is inside an HTML tag.
    ```html
    <p style="font-size: 120%">
    ```
1. CSS code is inside an HTML document.
    ```html
    <style>
        p {
            font-size: 120%
        }
    </style>
    ```
1. CSS code in external file. e.g. you can use `style.css` in HTML by using `<link rel="stylesheet" type="text/css" href="style.css">`.
    ```css
    p {
        font-size: 120%
    }
    ```

In CSS, inheritance controls what happens when no value is specified for a property on an element.There are 2 types:
- *Inherited properties*, which by default are set to the computed value of the parent element. e.g. `color`.
- *Non-inherited properties*, which by default are set to initial value of the property. e.g. `border`.

We can use `id` or `class` in HTML tag to specify. `id` cannot be reused but `class` can be. e.g. in CSS, you can use `#author-text` to select `<p id="author-text">` tag and `.main-text` to select `<p class="main-text">`.

### Box Model
<img src="./screenshots/box-model.png" alt="box-model"/>

The CSS box model is essentially a box that wraps around every HTML element. It consists of:
- *Content*: The content of the box, where text and images appear.
- *Padding*: Clears an area around the content. The padding is transparent.
- *Border*: A border that goes around the padding and content.
- *Margin*: Clears an area outside the border. The margin is transparent.

Notice that the `box-sizing: border-box` allows us to include the *padding* and *border* in an element's total width and height.

### Simple Layout
Now we can use `<div class="container">` to include the content that we want to arrange. See following HTML and CSS, notice that we need to add `.clearfix:after` to clear floats. Otherwise, `author-box` will be under the `other-post` instead of the bottom of `blog-post`.
```html
<div class="container">
    <div class="blog-post">...</div>
    <div class="other-post">...</div>
</div>
<div class="clearfix"></div>
<div class="author-box">
    ...
</div>
```

```css
.container {
    width: 1140px;
    margin: 20px auto 0 auto;
}

.blog-post {
    width: 75%;
    float: left;
    padding-right: 30px;
    position: relative;
}

.other-post {
    width: 25%;
    float: left;
}

.clearfix:after {
    content: "";
    display: table;
    clear: both;
}

.author-box {
    background-color: #ffff00;
    padding-top: 20px;
    border-top: 1px solid #a5a5a5;
}
```

In above example, we can also use `position: absolute` to determine the position of element. Check date position in HTML and CSS as follows:
```css
.date {
    position: absolute;
    top: 10px;
    right: 30px;
}
```


## Web Design

### Typography

1. Use a font-size between 15 and 25 pixels.
1. Use (really) big font sizes for headings. You may need to decrease font weight sometimes to ensure that the text doesn't steal too much attention.
1. Use line spacing between 120% and 150%.
1. 45 to 90 characters per line.
1. Use good fonts (sans-serif or serif).

### Color

1. Use only one base color.
    - Choose your base color by [Flat UI Colors](https://flatuicolors.com/).
    - Create your color palette based on base color by [0to255](https://www.0to255.com/)
1. Use a tool (e.g. Adobe Color CC or Paletton) if you want to use more colors (mix colors).
1. Use color to draw attention.
1. ***NEVER*** use Black in your design.
1. Choose colors wisely (more details see color theory). 
    - **Red** is a great color to use when power, passion, strength and excitement want to be transmitted. Brighter tones are more energetic and darker shades are more powerful and elegant.
    - **Orange** draws attention without being as overpowering as red. It means cheerfulness and creativity. Orange can be associated with friendliness, confidence, and courage.
    - **Yellow** is energetic and gives the feeling of happiness and liveliness. Also, it associates with curiosity, intelligence, brightness, etc.
    - **Green** is the color of harmony, nature, life and health. Also, it is often associated with money. In design, green can have a balancing and harmonizing effect.
    - **Blue** means patience, peace, trustworthiness, and stability. It is one of the most beloved colors, especially by men. It is associated with professionalism, trust and honor. That's actually why the biggest social networks use blue.
    - **Purple** is traditionally associated with power, nobility and wealth. In your design, purple can give a sense of wisdom, royalty, nobility, luxury, and mystery.
    - **Pink** expresses romance, passivity, care, peace, affection, etc.
    - **Brown** is the color of relaxation and confidence. Brown means earthiness, nature, durability, comfort, and reliability.

### Images

1. Put text directly on the image (this only works if the image is quite dark and your text is white).
1. Overlay the image with a color.
1. Put your text in a box.
1. Blur the image.
1. The floor fade (An image subtly fades towards black at the bottom).

### Icons

1. Use icons to list features/steps.
1. Use icons for actions and links.
    - Icons should be recognizable.
    - Label your icons if you have enough space to do so.
1. Icons should ***NOT*** take a center stage.
1. Use icon fonts whenever possible.

### Spacing and Layout

1. Use whitespace.
    - Put whitespace between your elements.
    - Put whitespace between your groups of elements.
    - Put whitespace between your website sections.
    - But don't exaggerate.
1. Define visual hierarchy.
    - Define where you want your audience to look first.
    - Establish a flow that corresponds to your content's message.
    - Use whitespace to build that flow.

### User Experience
User experience (UX) is the overall experience the user has with a product.
> *It's not just what it looks like and feels like.*<br>
> *Design is how it works.*<br>
> -- Steve Jobs

## 7 Steps for Fully Functional Website

### 1. Define your project
- Start off by **defining the goal** of your project. This can be showing your portfolio to the world, selling an e-book, building a blog, etc.
- Also **define your audience**. Ask yourself: which is the typical user that will visit my website?
- This is important, because you should always design with your goal and audience in mind.

### 2. Plan out everything
- Once your project is defined, plan your content carefully. This includes text, images, videos, icons, etc.
- Start thinking about **visual hierarchy**. It plays an important role when you start thinking about what you want on your website and what you don't. Defining the content before actually starting the design is called the *content-first approach*. It means that you should design for the content, instead of designing a webpage and then filling it with some stuff.
- **Define the navigation**.
- **Define the site structure**. You can draw a sitemap in this step if we're talking about a bigger project.

### 3. Sketch your ideas before you design

- Now it's time to get inspired and think about your design.
- Then, get the ideas out of your head. And with that I mean that you should sketch your ideas before you start designing. It will help you explore ideas and create a concept of what you want to build. Using pencil and paper is a great way of quickly retaining your valuable ideas.
- Make as many sketches as you want, but don't spend too much time perfecting anything. Once you have an initial idea, you can concentrate on the details when designing in HTML and CSS.
- ***NEVER*** start designing without having an idea of what you want to build. Getting inspiration is very important in this phase, and I already told you how to do that!

### 4. Design and develop your website

- After sketching, start to design your website using all the guidelines and tips you've learned in the web design section.
- You'll do that using HTML and CSS, which is called *designing in the browser*. Designing in the browser is basically designing and developing at the same time.
- There are more and more designers leaving traditional design programs such as Photoshop and start designing in the browser. The biggest reason for this is that you can't design responsive websites in photoshop. It also saves you tons of time.
In this phase, you'll use your sketches, content and planning decisions you've made in steps 1, 2 and 3.

### 5. It's not done yet: optimization

- Before you can actually launch your beautiful masterpiece for the world to see it, we have to optimize its performance in terms of **site speed**.
- You also need to do some basic **search engine optimization (SEO)** for search engines such as google.

### 6. Launch the masterpiece

- Your optimized website is now finally ready to launch.
- All you need for launching is a webserver that will host your website and deliver it to the world.

### 7. Site maintenance

- The launch of your website is not the end of the story.
- Now it's time to monitor your users' behavior and make some changes to your website if necessary.
- You should also update your content regularly in order to show your users that your website is alive! For instance, a blog can be a great way of doing that.

## Project: Omnifood
For this section, please refer to [Omnifood](omnifood/). The working directory of following tutorial is [Omnifood](omnifood/) as well.
### Prerequisites for Omnifood
1. Define your project
    - Project goal: show what Omnifood does, how it works and in which cities it operates in. Then, make people sign up on a subscription plan.
    - Audience (Targets): busy people who don't have time to cook or don't like it.
1. Plan out the website
    - Content: provided by Omnifood in form of a text document and a couple of imags.
    - Navigation: provided by Omnifood in a text of document.
1. Sketch your ideas
1. Design
    - Color: **Orange**.
    - Font: **Lato**.

### CSS Settings
1. Please refer to [Omnifood](omnifood/).
1. Visit [Normalize.css](https://necolas.github.io/normalize.css/) and save the `normalize.css` under [vendors/css/](omnifood/vendors/css/).
1. Include `normalize.css` in [index.html](omnifood/index.html).
1. Go to [Google Font]() and query `Lato`.
1. Choose **Thin 100**, **Light 300**, **Light 300 italic** and **Regular 400**.
    <img src="screenshots/lato.png" alt="lato"/>
1. Change tag in right panel to **Embed** and copy the script. Then, paste it to `index.html`.
    ```html
    <link href="https://fonts.googleapis.com/css2?family=Lato:ital,wght@0,100;0,300;0,400;1,300&display=swap" rel="stylesheet">
    ```

### Responsive Web Design (RWD)

We are going to use [grid.css](omnifood/vendors/css/grid.css) to build a responsive website. So include the `grid.css` in your `index.html`. Then set up CSS in [style.css](omnifood/resources/css/style.css).
```css
.row {
    max-width: 1140px;
    margin: 0 auto;
}
```
1. **Fluid grid**: all layout elements are sized in relative unites, such as percentages, instead of absolute units like pixels.
1. **Flexible images**: are also sized in relative unites.
1. **Media queries**: allow us to specify different CSS style rules for different browser widths.

### Header

- Background image
    1. Go to [style.css](omnifood/resources/css/style.css) file.
    1. Add image path in `header`:
        ```css
        header {
            background-image: url(img/hero.jpg);
        }
        ```
    1. Now you can't see any image, that's because the image only fills the part that has some content. So we have to use following setting. `vh` is relative to 1% of the height of the viewport which means the browser window size. So we set up to `100vh` to fill out whole window.
        ```css
        header {
            background-image: url(img/hero.jpg);
            height: 100vh;
        }
        ```
    1. We also need to put the image to center and adjust to suitable size. `cover` is to scale the image as large as possible without stretching the image.
        ```css
        header {
            background-image: url(img/hero.jpg);
            background-size: cover;
            background-position: center;
            height: 100vh;
        }
        ```
    1. Now notice that we still have some margin on the top, that's because of the normalized CSS file. So we need to get rid of it.
        ```css
        h1 {
            margin: 0;
        }
        ```
    1. Header should be in center. Notice that we need to use `transform` to put the header to right place.
        ```css
        .hero-text-box {
            position: absolute;
            width: 1140px;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
        }
        ```
    1. The last thing is the image is too light to be easy to read the text. We need to make image darker. Actually we don't need gradient, all we need is making image darker. So we use `rgba(0, 0, 0, 0.7)` to itself.
        ```css
        header {
            background-image: linear-gradient(rgba(0, 0, 0, 0.7), rgba(0, 0, 0, 0.7)), url(img/hero.jpg);
        }
        ```
- Text and button
    1. Go to [style.css](omnifood/resources/css/style.css) file.
    1. Update your `h1` class.
        ```css
        h1 {
            margin-top: 0;
            margin-bottom: 20px;
            color: #fff;
            font-size: 240%;
            font-weight: 300;
            text-transform: uppercase;
            letter-spacing: 1px;
            word-spacing: 4px;
        }
        ```
    1. We want to add CSS style on buttons. Notice that we have 2 buttons on header, the classes are `btn btn-full` and `btn btn-ghost`. When we define `.btn`, we can control 2 of them. When defining `.btn-full` or `.btn-ghost`, we can only control 1 of them. Define `.btn` as follows.
        ```css
        .btn {
            display: inline-block;
            padding: 10px 30px;
            font-weight: 300;
            text-decoration: none;
            border-radius: 200px;
        }
        ```
    1. Now we define `btn-full` and `btn-ghost`.
        ```css
        .btn-full {
            background-color: #e67e22;
            border: 1px solid #e67e22;
            color: #fff;
            margin-right: 15px
        }

        .btn-ghost {
            border: 1px solid #e67e22;
            color: #e67e22;
        }
        ```
    1. **Pseudo-class** is used to define a special state of an element. Now we introduce 4 common pseudo-classes used in `<a>` in usual. Please refer to [style.css](omnifood/resources/css/style.css).
        1. `a:link`: unvisited link.
        1. `a:visited`: visited link. 
        1. `a:hover`: mouse over link.
        1. `a:active`: selected link.
- Navigation bar: this part is similar to previous part, just notice:
    - We have 4 items in unordered list, we can use following setting on `<li>` to put them side by side:
        ```css
        .main-nav li {
            display: inline-block;
        }
        ```
    - We want to use `transition` effect. So we have to add border setting in `.main-nav li a:link` even if they cannot be seen (notice `transparent`).
        ```css
        .main-nav li a:link,
        .main-nav li a:visited {
            border-bottom: 2px solid transparent;
            transition: border-bottom 0.2s;
        }
        ```

### Features Section
- Text and icons
    1. Go to [index.html](omnifood/index.html) file.
    1. We have to define a section by `<section>`. In fact, this is just give a definition, there's no special effect.
    1. Define `<div class="row">` and `<div class="col span-1-of-4">`, this special naming please refer to downloaded [grid.css](omnifood/vendors/css/grid.css).
    1. Go to [Ionicons](https://ionicons.com/v2/) and download the icons.
    1. Unzip and copy **css/ionicons.min.css** file to `vendors/css/`. Copy all **fonts/** folder and replace `vendors/fonts`.
    1. Include the CSS file in `index.html`.
        ```html
        <link rel="stylesheet" type="text/css" href="vendors/css/ionicons.min.css">
        ```
    1. Now go back to [Ionicons](https://ionicons.com/v2/) and choose the icon you like and use `<i>` to use it.
    1. Notice that when you want to use special symbol like `--` (long dash), you need to use `&mdash;` to replace it. 
- Whitespace
    1. Go to [style.css](omnifood/resources/css/style.css) file.
    1. Define padding for section.
        ```css
        section {
            padding: 80px 0;
        }
        ```
    1. Define `h2` and `h3`, please see details in `style.css`.
    1. Now we want to insert a line after every `h2`. We can use so-called **pseudo-element** which is used to style specified parts of an element. We will use `h2::after` to insert some content after `h2`. Notice that you must give it `content`, even if we just want the effect of the yellow thin block(it's like a line).
        ```css
        h2::after {
            display: block;
            height: 2px;
            background-color: #e67e22;
            content: " ";
            width: 100px;
            margin: 0 auto;
            margin-top: 30px;
        }
        ```
    1. The paragraph below the yellow line, we want to put it center. We can give it 70% width and 15% margin left (then margin right will be 15%).
        ```css
        .long-copy {
            line-height: 145%;
            width: 70%;
            margin-left: 15%;
        }
        ```
    1. We want to change column style, but we cannot modify the original class `col span-1-of-4` which is from `grid.css`. So we have to add more class named `box` behind it.
        ```css
        .box {
            padding: 1%
        }
        ```
    1. Icons are too small, so we make it larger.
        ```css
        .icon-big {
            font-size: 350%;
            display: block;
            color: #e67e22;
            margin-bottom: 10%;
        }
        ```
### Favorite Meals Section
1. In this section, we are going to show 8 images. Go to [index.html](omnifood/index.html) file and add a new section. It's good way to use `<ul>` and `<li>` to arrange the images.
1. Go to [style.css](omnifood/resources/css/style.css) file.
1. We want to set up image side by side.
    ```css
    .meals-showcase li {
        display: block;
        float: left;
        width: 25%;
    }
    ```
1. We want the images more bigger in the beginning. So we have to scale it up.
    ```css
    .meal-photo img {
        width: 100%;
        height: auto;
        transform: scale(1.15);
    }
    ```
1. Now you notice that the image is overflow. So we set up `overflow: hodden`.
    ```css
    .meal-photo {
        width: 100%;
        margin: 0;
        overflow: hidden;
        background-color: #000;
    }
    ```
1. Now we set up the status when mouse is on it.
    ```css
    .meal-photo img:hover {
        transform: scale(1);
    }
    ```
1. Now you can find a small white space is on the bottom of images. There is a tricky skill to avoid it by setting scale more than 1 a little bit.
    ```css
    .meal-photo img:hover {
        transform: scale(1.03);
    }
    ```
1. We want the original image is darker. When mouse is on it, it will be more lighter.
    ```css
    .meal-photo img {
        opacity: 0.7;
        width: 100%;
        height: auto;
        transform: scale(1.15);
        transition: transform 0.5s, opacity 0.5s;
    }

    .meal-photo img:hover {
        opacity: 1;
        transform: scale(1.03);
    }
    ```
### How-it-Works Section
- Go to [style.css](omnifood/resources/css/style.css) file and see the following key points.
- There are 2 elements using class `steps-box`, if we want to control the first one, we can use `:first-child` selector. For second one, we can use `:last-child`.
    ```css
    .steps-box:first-child {
        text-align: right;
        padding-right: 3%;
        margin-top: 30px;
    }

    .steps-box:last-child {
        padding-left: 3%;
        margin-top: 70px;
    }
    ```
- If you want specify the tag, you should use `:last-of-type`. (The difference is still unclear for me...)
    ```css
    .works-step:last-of-type {
        margin-bottom: 80px;
    }
    ```
- You can notice that this section is close to previous section. That's because we are using floats, so `section-steps` starts with weird place, we need to clean those floats.
    ```css
    .clearfix {zoom: 1}
    .clearfix:after {
        content: '.';
        clear: both;
        display: block;
        height: 0;
        visibility: hidden;
    }
    ```
### Cities Section
- In this section, we focus on how align text and icon. Please see following code:
    ```css
    .icon-small {
        display: inline-block;
        width: 30px;
        text-align: center;
        color: #e67e22;
        font-size: 120%;
        margin-right: 10px;
        
        /* secret to align text and icons */
        line-height: 120%;
        vertical-align: middle;
        margin-top: -5px;
    }
    ```
- We want to style our link.
    ```css
    a:link,
    a:visited {
        color: #e67e22;
        text-decoration: none;
        padding-bottom: 1px;
        border-bottom: 1px solid #e67e22;
        transition: border-bottom 0.2s, color 0.2s;
    }

    a:hover,
    a:active {
        color: #555;
        border-bottom: 1px solid transparent;
    }
    ```
- Notice that the previous settings affect the class `btn-app`, so we need to fix it.
    ```css
    .btn-app:link,
    .btn-app:visited {
        border: 0;
    }
    ```
### Customer Testimonials Section
- We should put all background images under [resources/css/img](omnifood/resources/css/img/) folder instead of [resources/img](omnifood/resources/css/) folder.
- Try to use property `background-attachment: fixed;` in your background image.
    ```css
    .section-testimonials {
        background-image: linear-gradient(rgba(0, 0, 0, 0.8), rgba(0, 0, 0, 0.8)), url(img/back-customers.jpg);
        background-size: cover;
        color: #fff;
        background-attachment: fixed;
    }
    ```
- The good way to arrange the image and text is using `display: block;` property.
    ```css
    cite {
        font-size: 90%;
        margin-top: 25px;
        display: block;
    }

    cite img {
        height: 50px;
        border-radius: 50%;
        margin-right: 10px;
        vertical-align: middle;
    }
    ```
- Sometimes we need the special symbol in CSS, please refer to [here](https://css-tricks.com/snippets/html/glyphs/). For example, we want to show “ on your website. We can use property `content: "\201C";` like so:
    ```css
    blockquote:before {
        content: "\201C";
        font-size: 500%;
        display: block;
        position: absolute;
        top: -5px;
        left: -5px;
    }
    ```

### Sign Up Section
- If we want to mark up a part of a text, we can use `<span>` in HTML. Notice that the `<span>` is much like the `<div>`, but `<div>` is a **block-level element** and `<span>` is an **inline element**. `<span>` will inherit the parent attributes. So in the following case, if we use `font-size: 100%` in `.plan-price span`, the font size is 300% (20px * 300% = 60px) and same with `.plan-price`.
    ```css
    .plan-price {
        font-size: 300%;
        margin-bottom: 10px;
        font-weight: 100;
        color: #e67e22;
    }

    .plan-price span {
        font-size: 30%;
        font-weight: 300;
    }
    ```
- **Box shadow** is really common skill in CSS. The 4 properties in `box-shahow` mean x-offset, y-offset, blur and color.
    ```css
    .plan-box {
        background-color: #fff;
        border-radius: 5px;
        width: 90%;
        margin-left: 5%;
        box-shadow: 0 2px 2px #efefef;
    }

    .plan-box div:first-child {
        background-color: #fcfcfc;
        border-top-left-radius: 5px;
        border-top-right-radius: 5px;
    }
    ```

### Contact Form Section 
- For this section, we will use HTML form, please go to [index.html](omnifood/index.html) file and see more details. It's notable that we give `<input>` tag `id` property, that's because we want use it in `<label>` tag. We can use following code, the effect is when you click the label, then you can input the content directly. If you don't give property in element a value like `required`, the default setting is `required=true`.
    ```html
    <div class="col span-1-of-3">
        <label for="name">Name</label>                        
    </div>
    <div class="col span-2-of-3">
        <input type="text" name="name" id="name" placeholder="Your name" required>
    </div>
    ```
- If we want to control CSS of a particular type of `<input>`, we can use following code:
    ```css
    input[type=text],
    input[type=email] {
        width: 100%;
        padding: 7px;
        border-radius: 3px;
        border: 1px solid #ccc;
    }
    ```
- When we click `<input>` and prepare to input some content, we don't want the outline. We can use `:focus` to cancel this default setting.
    ```css
    *:focus {outline: none}
    ```

### Footer
The last section is nothing new, just refer to [index.html](omnifood/index.html) and [style.css](omnifood/resources/css/style.css) files.

## Responsive Web Design (RWD) with Media Queries

### Media Queries
The `@media` rule, introduced in CSS2, made it possible to define different style rules for different media types. **Media queries** in CSS3 extended the CSS2 media types idea. Instead of looking for a type of device, they look at the capability of the device.
- Media queries implementation
    1. Create a file [queries.css](omnifood/resources/css/queries.css) and put it under `resources/css` folder.
    1. Define media rule. Please check the following code and image.
        ```css
        /* Big tablets to 1200px(widths smaller than the 1140px row) */
        @media only screen and (max-width: 1200px) {
        }

        /* Small tablets to big tablets: from 768px to 1023px */
        @media only screen and (max-width: 1023px) {  
        }

        /* Small phones to small tablets: from 481 to 767px */
        @media only screen and (max-width: 767px) {    
        }

        /* Small phones: from 0 to 480px */
        @media only screen and (max-width: 480px) { 
        }
        ```
        <img src="screenshots/media-queries.jpg" alt="media-queries" />
    1. We need to import the `queries.css` in [index.html](omnifood/index.html).
        ```html
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" type="text/css" href="resources/css/queries.css">
        ```
    1. Open `index.html` by Chrome and open developer tool (`option + command + I`). Try to adjust the window width to 1200px and check this page to see which part is not fine.
    1. You can notice that when you decrease the width of window, the title will spill out the left side.
        ```css
        @media only screen and (max-width: 1200px) {
            .hero-text-box {
                width: 100%;
                padding: 0 2%;
            }
        }
        ```
    1. We notice that logo is too close to left side.
        ```css
        @media only screen and (max-width: 1200px) {
            .row {
                padding: 0 2%;
            }
        }
        ```
    1. So the standard process is to adjust your window and see which part is weird then you can set it up in `queries.css`. If your browser cannot scale below to specific pixel (like 400px or 500px) to test it, please try ([reference](https://stackoverflow.com/questions/8681903/browser-doesnt-scale-below-400px)):
        - Dock the web inspector to the right instead of to the bottom
        - Resize the inspector panel - you can now make the browser area really small (down to 0px).
### CSS Browser Prefixes
- CSS browser prefixes, are a way for browser makers to add support for new CSS features before those features are fully supported in all browsers. 
- If you are using [Brackets](http://brackets.io/) editor, please go to store and search `autoprefixer` and install it. Then select all (`Command + A`) CSS file and Choose **Edit** -> **Auto prefix selection** (`Shift + Command + P`).
- After using autoprefixer, you should try to use different browser to open your website for testing, e.g. IE, Safari, Firefox and so on.
- But for some older version of browser like IE6, 7, and 8, some modern CSS could not work on it.
    - `response.js` enables the browser to execute CSS media queries which we need for RWD. You can get the CDN [here](https://www.jsdelivr.com/package/npm/respond.js) (make sure to click HTML output). Go to your [index.html](omnifood/index.html) file and paste it.
        ```html
        <body>
            <script src="https://cdn.jsdelivr.net/respond/1.4.2/respond.min.js"></script>
        </body>
        ```
    - `html5shiv` enables us to use and style HTML5 elements in older browsers. You can get the CDN [here](https://www.jsdelivr.com/package/npm/html5shiv) (make sure to click HTML output). Go to your [index.html](omnifood/index.html) file and paste it.
        ```html
        <body>
            <script src="https://cdn.jsdelivr.net/npm/html5shiv@3.7.3/dist/html5shiv.min.js"></script>
        </body>
        ``` 
    - `selectivizr` allows us to use CSS3 pseudo classes on older browser. You can get the CDN [here](https://www.jsdelivr.com/package/npm/selectivizr) (make sure to click HTML output). Go to your [index.html](omnifood/index.html) file and paste it.
        ```html
        <body>
            <script src="https://cdn.jsdelivr.net/npm/selectivizr@1.0.3/selectivizr.min.js"></script>
        </body>
        ``` 
- You can go to [caniuse](https://caniuse.com/) to check if your element works on older brwoser or not. For example, you can type `transform` to see which browser supports this CSS style.

## jQuery

### Importing jQuery
- Useful resources to add some effects
    1. [Magnific Popup](https://dimsemenov.com/plugins/magnific-popup/) is a responsive lightbox & dialog script with focus on performance and providing best experience for user with any device.
    1. [Tooltipster](https://iamceege.github.io/tooltipster/) is a flexible and extensible jQuery plugin for modern tooltips.
    1. [Maplace.Js](http://ch-ny.com/content/themes/bridge-child/js/libs/maplace.js/) helps you to embed Google Maps into your website, quickly create markers and controls menu for the locations on map.
    1. [One Page Scroll](http://peachananr.github.io/onepage-scroll/Demo/demo.html) helps you create an Apple-like one page scroller website.
- Import jQuery from [Google Hosted libraries](https://developers.google.com/speed/libraries#jquery).
    ```html
    <body>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    </body>
    ```
- Create a file named `script.js` and put it under [resources/js/](omnifood/resources/js/) folder. This file is for jQuery script, now we import this file into [index.html](omnifood/index.html).
    ```html
    <body>
        <script src="resources/js/script.js"></script>
    </body>
    ```
### Sticky Navigation
1. Go to [style.css](omnifood/resources/css/style.css), we reserve the original navigation in section `MAIN NAVI` (we will use it in the following steps). For the sticky navigation, please see section `STICKY NAVI`. Owing to white background color, we have to use black logo, please check class `.logo-black`.
1. Go to [Waypoints](http://imakewebthings.com/waypoints/) and download this jQuery plugin. Unzip it, then copy **lib/jqyery.waypoints.min.js** and paste it under [vendors/js/](omnifood/vendors/js/). Then import id into [index.html](omnifood/index.html).
1. Go to [script.js](omnifood/resources/js/script.js) and add following code to control the `sticky` class.
    ```js
    $(document).ready(function() {
        $('.js--section-features').waypoint(function(direction) {
            if (direction == "down") {
                $('nav').addClass('sticky');
            } else {
                $('nav').removeClass('sticky');
            }
        }, {
        offset: '60px;'
        });    
    });
    ```
### Scrolling to Elements
1. We want an effect that enables us to click on a button to scroll to a specific element. Go to [index.html](omnifood/index.html).
    ```html
    <a class="btn btn-full js--scroll-to-plans" href="#">I'm hungry</a>
    <a class="btn btn-ghost js--scroll-to-start" href="#">Show me more</a>
    ...
    <section class="section-features js--section-features"></section>
    <section class="section-plans js--section-plans"></section>
    ```
1. Go to [script.js](omnifood/resources/js/script.js) and use following code.
    ```js
    /* Scroll on buttons */
    $('.js--scroll-to-plans').click(function() {
       $('html, body').animate({scrollTop: $('.js--section-plans').offset().top}, 1000); 
    });
    
    $('.js--scroll-to-start').click(function() {
       $('html, body').animate({scrollTop: $('.js--section-features').offset().top}, 1000); 
    });
    ```
1. Now we want to navigate to a specific element by click link. Notice that this smooth scrolling is using **jQuery 1.11**. You can see the latest version [here](https://css-tricks.com/snippets/jquery/smooth-scrolling/).
    ```js
    /* Navigation scroll */
    $(function() {
        $('a[href*=#]:not([href=#])').click(function() {
            if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
                var target = $(this.hash);
                target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
                if (target.length) {
                    $('html,body').animate({scrollTop: target.offset().top}, 1000);
                    return false;
                }
            }
        });
    });
    ```
1. Go back to your `index.html` and update class `main-nav`.
    ```html
    <ul class="main-nav">
        <li><a href="#features">Food delivery</a></li>
        <li><a href="#works">How it works</a></li>
        <li><a href="#cities">Our cities</a></li>
        <li><a href="#plans">Sign up</a></li>
    </ul>
    ```
1. Add new property for 4 sections.
    ```html
    <section class="section-features js--section-features" id="features">...</section>
    <section class="section-steps" id="works">...</section>
    <section class="section-cities" id="cities">...</section>
    <section class="section-testimonials" id="plans">...</section>
    ```
### Animations on Scroll
- You can find animations you like in [Animate.css](https://animate.style/).
- Fade in, fade in up, and pulse
    1. Download [animate.css](https://github.com/animate-css/animate.css/tree/a8d92e585b1b302f7749809c3308d5e381f9cb17).
    1. Put the `animate.css` under [vendors/css/](omnifood/vendors/css/) folder.
    1. Include `animate.css` in [index.html](omnifood/index.html).
        ```html
        <link rel="stylesheet" type="text/css" href="vendors/css/animate.css">
        ```
    1. Add following code in [script.js](omnifood/resources/js/script.js).
        ```js
        /* Animations on scroll */
        $('.js--wp-1').waypoint(function(direction) {
            $('.js--wp-1').addClass('animated fadeIn');
        }, {
            offset: '50%'
        });
        
        $('.js--wp-2').waypoint(function(direction) {
            $('.js--wp-2').addClass('animated fadeInUp');
        }, {
            offset: '50%'
        });
        
        $('.js--wp-3').waypoint(function(direction) {
            $('.js--wp-3').addClass('animated fadeIn');
        }, {
            offset: '50%'
        });
        
        $('.js--wp-4').waypoint(function(direction) {
            $('.js--wp-4').addClass('animated pulse');
        }, {
            offset: '50%'
        });
        ```
    1. For [style.css](omnifood/resources/css/style.css).
        ```css
        /* ----------------------------------- */
        /* ANIMATIONS */
        /* ----------------------------------- */

        .js--wp-1,
        .js--wp-2,
        .js--wp-3 {
            opacity: 0;
            -webkit-animation-duration: 1s;
            animation-duration: 1s;
        }

        .js--wp-4 {
            -webkit-animation-duration: 1s;
            animation-duration: 1s;
        }

        .js--wp-1 .animated,
        .js--wp-2 .animated,
        .js--wp-3 .animated {
            opacity: 1;
        }
        ```
    1. Finally add effect by using class `js--wp-1`, `js--wp-2`, `js--wp-3`, and `js--wp-4` whatever you want in `index.html`.

### Navigation Responsive
In this section, we want to show burger menu in mobile device instead of original navigation.
1. Go to [script.js](omnifood/resources/js/script.js) file.
    ```js
    /* Mobile navigation */
    $('.js--nav-icon').click(function() {
        var nav = $('.js--main-nav');
        var icon = $('.js--nav-icon i');
        
        nav.slideToggle(200);
        
        if (icon.hasClass('ion-navicon-round')) {
            icon.addClass('ion-close-round');
            icon.removeClass('ion-navicon-round');
        } else {
            icon.addClass('ion-navicon-round');
            icon.removeClass('ion-close-round');
        }        
    });
    ```
1. Go to [style.css](omnifood/resources/css/style.css) file.
    ```css
    /* ----- MOBILE NAVI ----- */
    .mobile-nav-icon {
        float: right;
        margin-top: 30px;
        cursor: pointer;
        display: none;
    }

    .mobile-nav-icon i {
        font-size: 200%;
        color: #fff;
    }
    ```
1. Go to [queries.css](omnifood/resources/css/queries.css) file.
    ```css
    @media only screen and (max-width: 767px) {
        /* Mobile navigation */
        $('.js--nav-icon').click(function() {
            var nav = $('.js--main-nav');
            var icon = $('.js--nav-icon i');
            
            nav.slideToggle(200);
            
            if (icon.hasClass('ion-navicon-round')) {
                icon.addClass('ion-close-round');
                icon.removeClass('ion-navicon-round');
            } else {
                icon.addClass('ion-navicon-round');
                icon.removeClass('ion-close-round');
            }        
        });
    }
    ```
1. Go to [index.html](omnifood/index.html) file.
    ```html
    <header>
        <nav>
            <div class="row">
                <img src="resources/img/logo-white.png" alt="Omnifood logo" class="logo">
                <img src="resources/img/logo.png" alt="Omnifood logo" class="logo-black">
                <ul class="main-nav js--main-nav">
                    <li><a href="#features">Food delivery</a></li>
                    <li><a href="#works">How it works</a></li>
                    <li><a href="#cities">Our cities</a></li>
                    <li><a href="#plans">Sign up</a></li>
                </ul>
                <a class="mobile-nav-icon js--nav-icon"><i class="ion-navicon-round"></i></a>
            </div>
        </nav>   
    </header>    
    ```

## Optimization and Launching website

### Favicon
1. Go to [Favicon Generator](https://realfavicongenerator.net/) to create your favicon. Select your favicon image and upload your logo.
1. For the **Favicon Generator Options** section, please input the path as follows. Then click **Generate Favicon**.
    <img src="screenshots/favicon-generator.png" alt="favicon-generator">
1. Create a folder under [resources/](omnifood/resources/) named `favicons`.
1. After generating, you have to download the package (in item 1) and copy following code (in item 3) to your [index.html](omnifood/index.html).
    <img src="screenshots/favicon-code.png" alt="favicon-code">
    ```html
    <head>
        <link rel="apple-touch-icon" sizes="180x180" href="/resources/favicons/apple-touch-icon.png">
        <link rel="icon" type="image/png" sizes="32x32" href="/resources/favicons/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="16x16" href="/resources/favicons/favicon-16x16.png">
        <link rel="manifest" href="/resources/favicons/site.webmanifest">
        <link rel="mask-icon" href="/resources/favicons/safari-pinned-tab.svg" color="#5bbad5">
        <link rel="shortcut icon" href="/resources/favicons/favicon.ico">
        <meta name="msapplication-TileColor" content="#da532c">
        <meta name="msapplication-config" content="/resources/favicons/browserconfig.xml">
        <meta name="theme-color" content="#ffffff">
    </head>
    ```
1. Unzip your downloaded file, and put all stuffs under [favicons/](omnifood/resources/favicons/) folder.
1. Open `index.html` by browser then you can see the favicon.

### Site Speed by Optimizing Heavy Images
1. Open your [index.html](omnifood/index.html) by Chrome. You can find out the pixel of the London image as follows. The idea is we just use the small size of image, so we can decrease the dimention of the iamge.<br>
    <img src="screenshots/london-1.png" alt="london-1" width=300px/>
1. But remember, we are using RWD. In some small screen size, this picture will be bigger. Now we focus on screen with 400px width. The width of this image will be as follows.<br>
    <img src="screenshots/london-2.png" alt="london-2" width=450px>
1. We are using a little bigger size of 360px (in previous step), say 375px. We want the image size to be always **twice** as much as the actual size (375px), that's for some high resolution mobiles. So we are going to use 750px (375px * 2) to be our width.
1. Choose 4 cities image and open it by default.
1. Select all 4 images and click **Adjust Size**.
1. Input `750 pixels` in width.
    <img src="screenshots/adjust-size.png" alt="adjust-size">
1. Close and save all.
1. Go back to website and refresh it, you can see the intrinsic size of the image decreases to 750px by 500px.<br>
    <img src="screenshots/london-3.png" alt="london-3" width=300px>
1. You can do above steps to reduce the dimenstion of all images.
1. But for some very big images (e.g. background), we need to do some compress online. Go to [Optimizilla](https://imagecompressor.com/) and upload the background images which is under [resources/css/img/](omnifood/resources/css/img/) folder.
1. After uploading, choose the qualify we want. We apply **70** quality and click **Apply** (for image **hero.jpg**, we apply **89** quality). 
    <img src="screenshots/compress.png" alt="compress">
1. Download compressed images and put them under [resources/css/img/](omnifood/resources/css/img/) folder.
1. Change the background image path in [style.css](omnifood/resources/css/style.css).

### Basic Search Engine Optimization (SEO)

SEO is a technique that improves and promotes a website to increase the number of visitors the site receives from search engine.

1. Meta description tag: is a short description of a website and used to describe a website. You should use important keywords but with no more than 160 characters.
    ```html
    <meta name="description" content="Omnifood is a premium food delivery service with the mission to bring affordable and healthy meals to as many people as possible.">
    ```
1. HTML validation: valid HTML code which is prefered by Google is code that follows exactly the official HTML rules and has no bugs. We can check our HTML on [W3C validator](https://validator.w3.org/).
    1. Copy your all HTML code.
    1. Paste it on [Validate by Direct Input](https://validator.w3.org/#validate_by_input) and click **Check**.
    1. Try to handle all errors.
1. Content is king.
1. Keyword: use keywords in the title, meta description tag, headings and links. Don't overuse keywords, since search engine consider that as keyword spamming and might penalize you.
1. Backlinks: is to get other websites to link to you.

### Launching a webpage

1. Choose and buy a domain name.
1. Buy web hosting.
1. Upload the website.

### Google Analytics (GA)

Use GA to trace the user behavior and make your website better.

## Flexbox

Flexbox makes it easier to design flexible responsive layout structure without using float or positioning.

### BEM
- The **Block**, **Element**, **Modifier** methodology (commonly referred to as **BEM**) is a naming convention for classes in HTML and CSS. 
- Please go to [flexbox/index.html](flexbox/index.html) file. Block here is pizza, and then each of the elements in there will have the block name on them. We are using 2 underscore between block and element.
    ```html
    <figure class="pizza">
        <div class="pizza__hero">
    </figure>
    ```
- If you have multiple elements have same name, but you want one of them to be slightly different, we'll use 2 dashes between them.
    ```html
    <div class="pizza__tag pizza__tag--1">#vegetarian</div>
    <div class="pizza__tag pizza__tag--2">#italian</div>
    ```
### Flexbox
- We define the parent container as flexbox. Please refer to [flexbox/style.css](lexbox/style.css). All the elements under this container will appear side by side.
    ```css
    .pizza {
        display: flex;
    }
    ```
- There are 3 important properties of `flex`.
    1. `flex-grow`: specifies how much a flex item will grow relative to the rest of the flex items. The default value is 0.
    1. `flex-shrink`: specifies how much a flex item will shrink relative to the rest of the flex items. The default value is 1.
    1. `flex-basis`: specifies the initial length of a flex item.
- Example of `flex`. For `.pizza__content`, `1` means this element will occupy the rest of space. 
    ```css
    .pizza__hero {
        flex: 0 0 45%;
    }

    .pizza__content {
        flex: 1;
    }

    .pizza__price {
        flex: 0 0 50px;
    }
    ```
- There are 2 axises of flex, one is main axis (X-axis), another is cross axis (Y-axis). We can use `align-items: center` to align elements along the cross axis. You can also use following value: `flex-start`, `flex-end`, `stretch` and so on.
    ```css
    .pizza {
        display: flex;
        align-items: center;
    }
    ```
- If you don't want all elements aligned to same position, you can use `align-self` to control individual element.
    ```css
    .pizza__price {
        align-self: flex-start;
    }
    ```
- We can use `justify-content` to align main axis. Notice that you need to define the element as `display: flex` first, then you can use `align-items` and `justify-content`.
    ```css
    body {
        display: flex;
        justify-content: center;
    }
    ```
- If you change the text direction, then you change the main and cross axis.
    ```css
    .pizza__price {
        writing-mode: vertical-rl;
        
        display: flex;
        align-items: center;
        justify-content: center;
    }
    ```
- We have many elements in `.pizza__content`, we can use `flex` to control them alignment more easily.
    ```css
    .pizza__content {
        display: flex;
        flex-direction: column;
    }
    ```